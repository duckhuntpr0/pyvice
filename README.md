#PyVice

Python playing Commodore (64)

+ Port 6500 is an open [VICE monitor](http://vice-emu.sourceforge.net/vice_12.html) telnet port
+ Port 5900 is X11VNC  **!!! WARNING: IT'S OPEN TO INPUT !!!**

##Build
+ run *./docker_build.sh* . This will build 'pyvice' from *Dockerfile*

##Start

+ run *./docker_shell_in.sh* . This will run a tmux session where pane 1 is a shell and pane 2 shows supervisord . CTRL + b & d, to end.

##Telnet

To telnet into a running VICE monitor (e.i by using a linux terminal):

+ **telnet 172.17.0.2 6500** *(IP might vary)*
+ Enter '?' to get a list of the commands available
	
##Notes

	http://vice-emu.sourceforge.net/vice_6.html#SEC84
	http://vice-emu.sourceforge.net/vice_12.html
	http://supervisord.org/
	http://materm.sourceforge.net/wiki/pmwiki.php
	https://stackoverflow.com/questions/10952514/telnetlib-python-example

	http://www.lemon64.com/forum/viewtopic.php?t=55750
	http://vice-emu.sourceforge.net/vice_6.html#SEC81
	https://www.c64-wiki.com/wiki/Main_Page
	https://www.c64-wiki.com/wiki/CPU
	http://codebase64.org/doku.php?id=base:using_the_vice_monitor
	http://project64.c64.org/hw/c64.html
	https://archive.org/stream/The_Master_Memory_Map_for_the_Commodore_64#page/n63/mode/2up


	https://stackoverflow.com/questions/27637465/record-local-audio-in-a-docker-container

	https://sourceforge.net/p/vice-emu/code/HEAD/tree/trunk/vice/src/sounddrv/soundpulse.c
	https://github.com/pulseaudio/pulseaudio/blob/master/src/pulse/simple.h
	https://superuser.com/questions/1339038/how-can-i-can-create-a-virtual-pulseaudio-soundcard-on-linux-with-no-audio-hardw
