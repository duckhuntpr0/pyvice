FROM alpine:latest
#FROM alpine:3.7


LABEL maintainer="DuckHP"
LABEL description="Commodore(64) playing AI"
LABEL url="https://bitbucket.org/duckhuntpr0/pyvice"


ENV BASE_RUNTIME_REQUIREMENTS dbus dbus-x11 git python3 tmux xvfb supervisor fluxbox terminator feh openssl ffmpeg pulseaudio@edgecommunity pulseaudio-utils@edgecommunity json-c@edge
#pulseaudio@edgecommunity json-c@edge musl-dev@edge musl@edge musl-utils@edge
#alsa-utils alsa-utils-doc alsa-lib alsaconf

ENV X11VNC_VERSION 0.9.15
ENV X11VNC_BUILD_REQUIREMENTS musl-dev@edge musl-utils@edge musl@edge build-base openssl-dev libjpeg-turbo-dev libvncserver-dev zstd-dev automake autoconf
ENV X11VNC_CONFIGURE_ARGS --without-macosx-native --without-xinerama --without-xtrap --without-xfixes --without-xcomposite --without-avahi --without-v4l
ENV X11VNC_PORT 5900

ENV VICE_BUILD_REQUIREMENTS build-base byacc flex libxaw-dev readline-dev ffmpeg-dev libpcap-dev pulseaudio-dev@edgecommunity
ENV VICE_CONFIGURE_ARGS --with-pulse --enable-cpuhistory --enable-ethernet --disable-realdevice --disable-hardsid --disable-ssi2001 --disable-parsid --disable-catweasel --disable-textfield --disable-lame --disable-editline --disable-realdevice --disable-nls --disable-bundle --disable-hidutils -disable-hidmgr --disable-lame --with-x --enable-platformdox --enable-external-ffmpeg

ENV RAMDISK_SIZE 512k

WORKDIR /app
COPY ./src/boot.sh /app/boot.sh
COPY ./src/vice-3.2.tar.gz /app/vice.tar.gz
COPY ./src/xa-2.3.8.tar.gz /app/xa.tar.gz
COPY ./src/requirements.txt /app/
COPY ./src/fs/etc/supervisord.conf /etc/
COPY ./src/fs/home/pyvice /home/pyvice

RUN echo '@edge http://nl.alpinelinux.org/alpine/edge/main' >> /etc/apk/repositories && \
	echo '@edgecommunity http://nl.alpinelinux.org/alpine/edge/community' >> /etc/apk/repositories && \
	echo '@testing http://nl.alpinelinux.org/alpine/edge/testing' >> /etc/apk/repositories && \
	apk update --progress && \
	apk add --upgrade apk-tools@edge && \
    apk add --progress --update ${BASE_RUNTIME_REQUIREMENTS} && \
    pip3 install --upgrade pip && \
    pip3 install --upgrade wheel && \
    addgroup pyvice && \
    adduser -G pyvice -s /bin/sh -D pyvice && \
    adduser pyvice audio && \
    adduser root audio && \
    echo 'pyvice:pyvice' | /usr/sbin/chpasswd && \
    chmod +x /app/boot.sh && \
    echo "***** INSTALLING X11VNC $X11VNC_VERSION AND REQUIREMENTS *****" && \
    apk add --progress --update ${X11VNC_BUILD_REQUIREMENTS} && \
    cd /app && git clone -q -n --progress --depth 1 --single-branch --branch ${X11VNC_VERSION} https://github.com/LibVNC/x11vnc.git && \
    cd x11vnc && git checkout ${X11VNC_VERSION} && \
    autoreconf -v --install && \
    ./configure --prefix=/usr --sysconfdir=/etc --mandir=/usr/share/man --infodir=/usr/share/info --localstatedir=/var ${X11VNC_CONFIGURE_ARGS} && \
    make && make install-exec && \
    cd /app && \
    echo "***** INSTALLING VICE AND REQUIREMENTS *****" && \
    apk add --progress --update ${VICE_BUILD_REQUIREMENTS} && \
    cd /app && \
    tar xvzf xa.tar.gz && cd xa-2.3.8 && make && make install && \
    cd /app/ && \
    tar xvzf vice.tar.gz && \
    cd vice-3.2 && \
    ./configure --prefix=/usr --sysconfdir=/etc --mandir=/usr/share/man --infodir=/usr/share/info --localstatedir=/var ${VICE_CONFIGURE_ARGS} && \
    echo echo make && make install && \
    echo "***** SETTING UP PYVICE HOME FOLDER *****" && \
    chown -v -R pyvice:pyvice /home/pyvice/
    
COPY ./src/roms /home/pyvice/roms

#VOLUME ["/data"]
#WORKDIR /data
#EXPOSE 29000
EXPOSE 5900
EXPOSE 6500
#CMD ["/bin/sh"]
CMD ["/app/boot.sh"]
