#!/bin/sh
#echo "***** MOUNTING A $RAMDISK_SIZE RAMDISK AT /mnt/ramdisk FOR QUICK R/W THINGS*****"
#mkdir /mnt/ramdisk
#mount -t tmpfs -o size=1M tmpfs /mnt/ramdisk
tmux new-session -s 'PyVice' -n 'PyVice' -d '/bin/sh'
tmux split-window -v 'supervisord -c /etc/supervisord.conf'
tmux select-pane -t 0
tmux -2 attach-session -d
